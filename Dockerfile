FROM java:8

RUN apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv EA312927
RUN echo "deb http://repo.mongodb.org/apt/debian wheezy/mongodb-org/3.2 main" | tee /etc/apt/sources.list.d/mongodb-org-3.2.list
RUN apt-get update && apt-get -y install mongodb-org

ENV UNIFI_VERSION 5.0.7
ENV UNIFI_PATH /unifi
RUN mkdir $UNIFI_PATH
WORKDIR $UNIFI_PATH
RUN wget http://dl.ubnt.com/unifi/$UNIFI_VERSION/UniFi.unix.zip
RUN unzip UniFi.unix.zip

EXPOSE 8080/tcp
EXPOSE 8081/tcp
EXPOSE 8443/tcp
EXPOSE 8843/tcp
EXPOSE 8880/tcp
EXPOSE 3478/udp
# EXPOSE 27117
# EXPOSE 8881
# EXPOSE 8882

# volume /unifi/UniFi/data/
CMD java -jar -Xmx1024M $UNIFI_PATH/UniFi/lib/ace.jar start
